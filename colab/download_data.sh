#!/bin/bash

# Downloads data and unpack into the $PWD/dataset location
# You should acquire download links from the official data provider

set -e

# fill the array with proper urls
download_url=()

WORKDIR=$PWD
JESTER_DATA_STORAGE="$WORKDIR/dataset"

mkdir -p $JESTER_DATA_STORAGE
cd $JESTER_DATA_STORAGE

for url in "${download_url[@]}"; do
  file_name=$(basename $url | cut -d '?' -f 1)
  echo "$file_name"
  wget --no-clobber --show-progress --output-document "$JESTER_DATA_STORAGE/$file_name" $url
done

cat 20bn-jester-v1-?? | tar zx
