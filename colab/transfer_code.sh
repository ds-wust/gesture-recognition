#!/bin/bash

# Directories synchronization with, e.g. GDrive
# You have to have rclone installed and configured
# Script may fail due to rclone when you reached API call limit, e.g. API call limit on GDrive

set -e

if [[ $# -lt 2 ]] ; then
    echo 'You have to provide <source_path> <dest_path> arguments'
    exit 0
fi

LOCAL_SOURCE_CODE_FOLDER=$1
DRIVE_SOURCE_CODE_FOLDER=$2

echo "Synchronizing directory: $LOCAL_SOURCE_CODE_FOLDER with: $DRIVE_SOURCE_CODE_FOLDER"

read -r -p "Are you sure? [y/N] " response
case "$response" in
    [yY][eE][sS]|[yY]) ;;
    *)
        exit 0
        ;;
esac

rclone sync "$LOCAL_SOURCE_CODE_FOLDER" "$DRIVE_SOURCE_CODE_FOLDER" \
--exclude=.idea/ \
--exclude=.mypy_cache/ \
--exclude=.pytest_cache/ \
--exclude=.tox/ \
--exclude=.git/ \
--exclude=__pycache__/ \
--exclude=.coverage \
--exclude=.ipynb_checkpoints/ \
--exclude=colab/ \
--exclude=config/ \
--progress