#!/usr/bin/env bash
set -e

if [[ -z "$2" ]]; then
  echo "Image name not provided"
  exit 1
fi

DEFAULT_VERSION="1.0"

dockerfile_path="$1"
context=$(dirname "$dockerfile_path")
image_name="$2"
username="$3"
version=${4:-$DEFAULT_VERSION}

echo "Building $image_name:$version from $dockerfile_path at context: $context"

(docker build -f "$dockerfile_path" -t "$image_name":"$version" -t "$image_name":latest .)
docker login --username="$username"
docker tag "$image_name":latest "$username"/"$image_name":"$version"
docker tag "$image_name":latest "$username"/"$image_name":latest
docker push "$username"/"$image_name":"$version"
docker push "$username"/"$image_name":latest
