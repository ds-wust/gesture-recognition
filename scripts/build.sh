#!/usr/bin/env bash
sudo docker build . -t mateuszdockeraprzeloz/gestureclassic:latest
cd ..
sudo docker run -it \
 -e GRCM_DATA_PATH='/data' \
 -e GRCM_LIMIT_TRAIN='10000' \
 -e GRCM_LIMIT_VAL='100' \
 -e GRCM_CLASS_SUBSET='None' \
 -e GRCM_TARGET_SHAPE='(176, 96)' \
 -e GRCM_CENTER_CROP='(100, 100)' \
 -e GRCM_TRAIN_BATCH_SIZE='10000' \
 -e GRCM_CLF_NAME='RandomForest' \
 -e GRCM_CLF_ARGS='{}' \
 -e GRCM_TRAIN_FROM_PREPROCESSED='True' \
 -e GRCM_IS_INSIDE_DOCKER='True' \
 -v /home/ubuntu/storage:/storage \
 -v /home/ubuntu/output:/output \
 -v /home/ubuntu/dataset:/data mateuszdockeraprzeloz/gestureclassic bash \
 -c 'PYTHONPATH=/app/app python3 app/gesture_recognition/classic_model/experiment.py'