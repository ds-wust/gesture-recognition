import ast
import glob
import os
import pickle as pkl
from pathlib import Path
from typing import List, Tuple, Dict, Union

import cv2
import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.linear_model import SGDClassifier, PassiveAggressiveClassifier
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC

from gesture_recognition.classic_model.utils import LABELS_MAPPING
from settings import OUTPUT_PATH, TRAIN_PKL_PATH

classifiers = {
    'SVM': SVC,
    'RandomForest': RandomForestClassifier,
    'SGD': SGDClassifier,
    'PA': PassiveAggressiveClassifier,
    'KNN': KNeighborsClassifier,
    'ADB': AdaBoostClassifier
}


def read_image_list(path, mode=0):
    images = sorted(glob.glob(f"{path}/*.jpg"))
    return [cv2.imread(img_path, mode) for img_path in images][::2]


def center_crop(img: np.array, width: int, height: int):
    center = img.shape[0] / 2, img.shape[1] / 2
    x = center[1] - width / 2
    y = center[0] - height / 2

    return img[int(y):int(y + height), int(x):int(x + width)]


def center_crop_images(image_list: List[np.array], size: Tuple[int, int]):
    return [center_crop(img, size[0], size[1]) for img in image_list]


def resize_images(image_list: List[np.array], target_shape: Tuple[int, int] = (176, 96)) -> List[
    np.array]:
    return [cv2.resize(img, target_shape) for img in image_list]


def get_sum_optical_flow(image_list: List[np.array], flatten: bool = False) -> np.array:
    # optical_flow = []

    # hsv_mask = np.zeros_like(image_list[0])
    image_shape = np.shape(image_list[0])
    hsv_mask_shape = (image_shape[0], image_shape[1], 3)
    hsv_mask = np.zeros(hsv_mask_shape)
    # Make image saturation to a maximum value
    hsv_mask[..., 1] = 255

    flow_sum = np.zeros(shape=(image_list[0].shape[0], image_list[0].shape[1]))

    for index in range(len(image_list) - 1):
        img_prev = image_list[index]
        img_next = image_list[index + 1]

        # img_prev = cv2.cvtColor(img_prev, cv2.COLOR_BGR2GRAY)
        # img_next = cv2.cvtColor(img_next, cv2.COLOR_BGR2GRAY)

        flow = cv2.calcOpticalFlowFarneback(img_prev, img_next, None, 0.5, 3, 10, 3, 5, 1.2, 0)

        # Compute magnite and angle of 2D vector
        mag, ang = cv2.cartToPolar(flow[..., 0], flow[..., 1])
        # Set image hue value according to the angle of optical flow
        hsv_mask[..., 0] = ang * 180 / np.pi / 2
        # Set value as per the normalized magnitude of optical flow
        hsv_mask[..., 2] = cv2.normalize(mag, None, 0, 255, cv2.NORM_MINMAX)
        # Convert to rgb
        rgb_representation = cv2.cvtColor(np.float32(hsv_mask), cv2.COLOR_HSV2BGR)
        # Convert to grayscale
        gray_representation = cv2.cvtColor(rgb_representation, cv2.COLOR_BGR2GRAY)

        flow_sum += gray_representation  # np.abs(gray_representation) * (index * 0.001)

        # optical_flow.append(gray_representation.flatten() if flatten else gray_representation)

    of_sum = scale_array(flow_sum.flatten())
    where_nan = np.isnan(of_sum)
    of_sum[where_nan] = 0
    return of_sum


def scale_array(arr: np.array) -> np.array:
    min_arr = np.min(arr)
    max_arr = np.max(arr)
    return (arr - min_arr) * (1 / (max_arr - min_arr + 0.00001))


def get_hog_descriptor(image: np.array, flat: bool = False) -> np.array:
    winSize = (64, 64)
    blockSize = (16, 16)
    blockStride = (8, 8)

    cellSize = (8, 8)
    nbins = 9
    derivAperture = 1
    winSigma = 4.
    histogramNormType = 0
    L2HysThreshold = 2.0000000000000001e-01
    gammaCorrection = 0
    nlevels = 64
    hog = cv2.HOGDescriptor(winSize, blockSize, blockStride, cellSize, nbins, derivAperture,
                            winSigma,
                            histogramNormType, L2HysThreshold, gammaCorrection, nlevels)
    winStride = (8, 8)
    padding = (8, 8)
    locations = ((10, 20),)
    hist = hog.compute(image, winStride, padding, locations)
    return np.squeeze(hist)


def get_hog_descriptors(image_list: List[np.array], flat: bool = False,
                        as_one_matrix=False) -> np.array:
    if as_one_matrix:
        return sum(image_list)

    return np.asarray([get_hog_descriptor(img, flat=flat) for img in image_list])  # map


def get_hogph(hog_arr: List[np.array]):
    hogph = hog_arr[0]
    for i in range(1, len(hog_arr)):
        hogph += hog_arr[i - 1] - hog_arr[i]

    return hogph


def read_labels(labels_path: Path):
    if not labels_path.exists():
        return {}
    df_labels = pd.read_csv(labels_path, sep=';', names=['index', 'label'])
    labels_data = [LABELS_MAPPING[label] for label in df_labels['label']]
    df_labels['class'] = labels_data
    return {str(row['index']): row['class'] for index, row in df_labels.iterrows()}


def get_dataset(videos_list: List[Tuple], labels: Dict, mode=cv2.IMREAD_GRAYSCALE, limit=None,
                class_subset=None, target_shape=(176, 96), center_crop=None):
    dataset = []
    dataset_labels = []
    for index, video_info in enumerate(videos_list):
        if index % 3000 == 0:
            print(f'Loading... {index}')
        video_index = video_info[0].split('/')[-1]

        if video_index not in labels.keys():
            continue

        if class_subset is not None:
            if labels[video_index] not in class_subset:
                continue

        video_path = video_info[0]
        images = read_image_list(video_path, mode=mode)

        if center_crop is not None:
            images = center_crop_images(images, center_crop)
        else:
            images = resize_images(images, target_shape=target_shape)

        # hog_descriptors = get_hog_descriptors(images, as_one_matrix=False)
        # hogph = get_hogph(hog_descriptors)

        optical_flow = get_sum_optical_flow(images, flatten=True)

        dataset_labels.append(labels[video_index])
        dataset.append(optical_flow)

    return dataset, dataset_labels


def save_to_pkl(object, path: str):
    with open(path, 'wb') as output:
        pkl.dump(object, output)


def from_pkl(path: str):
    with open(path, 'rb') as output:
        file = pkl.load(output)
    return file


def check_dimensions(arr):
    return np.atleast_2d(arr)


def load_datasets_from_pkls(paths: List[Union[str, Path]]):
    dataset = []
    dataset_labels = []
    for path in paths:
        x, y = from_pkl(path)
        dataset += x
        dataset_labels += y

    return dataset, dataset_labels


def create_dataset(videos_list: List[Tuple], labels: Dict, mode=cv2.IMREAD_GRAYSCALE, limit=None,
                   class_subset=None, target_shape=(176, 96), center_crop=None, batch_size=None,
                   from_preprocessed=False):
    if from_preprocessed:
        print('From preprocessed')
        saved_paths = glob.glob(f'{TRAIN_PKL_PATH}/*.pkl')
        return load_datasets_from_pkls(saved_paths)

    videos_list = videos_list[1:] if limit is None else videos_list[1:limit]
    dataset_len = len(videos_list)

    if batch_size is None or (batch_size is not None and batch_size >= dataset_len):
        return get_dataset(videos_list, labels, mode=mode, limit=limit, class_subset=class_subset,
                           target_shape=target_shape, center_crop=center_crop)

    prev_index = 0
    saved_paths = []
    for index in range(batch_size, dataset_len, batch_size):
        start = prev_index
        end = index
        print(f'Slice dataset to {start, end}')
        x, y = get_dataset(videos_list[start:end], labels, mode=mode, limit=limit,
                           class_subset=class_subset, target_shape=target_shape,
                           center_crop=center_crop)
        save_path = TRAIN_PKL_PATH / f'train_{start}-{end}.pkl'
        save_to_pkl((x, y), save_path)

        saved_paths.append(save_path)
        prev_index = index

    print(f'Data saved to paths: {saved_paths}')

    return load_datasets_from_pkls(saved_paths)


def get_classifier(clf_name: str):
    return classifiers[clf_name]


def read_env_variables():
    envs = {
        'GRCM_LIMIT_TRAIN': ast.literal_eval(
            os.getenv('GRCM_LIMIT_TRAIN', default='1000')
        ),
        'GRCM_CLASS_SUBSET': ast.literal_eval(
            os.getenv('GRCM_CLASS_SUBSET', default='[23, 0, 25, 20, 21, 1, 22]')
        ),
        'GRCM_TARGET_SHAPE': ast.literal_eval(
            os.getenv('GRCM_TARGET_SHAPE', default="None")
        ),
        'GRCM_CENTER_CROP': ast.literal_eval(
            os.getenv('GRCM_CENTER_CROP', default="(100, 100)")
        ),
        'GRCM_TRAIN_BATCH_SIZE': ast.literal_eval(
            os.getenv('GRCM_TRAIN_BATCH_SIZE', default="100")
        ),
        'GRCM_TRAIN_FROM_PREPROCESSED': ast.literal_eval(
            os.getenv('GRCM_TRAIN_FROM_PREPROCESSED', default="False")
        ),
        'GRCM_LIMIT_VAL': ast.literal_eval(
            os.getenv('GRCM_LIMIT_VAL', default='1000')
        ),
        'GRCM_CLF_NAME':
            os.getenv('GRCM_CLF_NAME', default="RandomForest"),
        'GRCM_DATA_PATH':
            os.getenv('GRCM_DATA_PATH', default='/home/mateusz/Downloads/class_subsample'),
        'GRCM_CLF_ARGS': ast.literal_eval(
            os.getenv('GRCM_CLF_ARGS', default="{}")
        ),
    }
    return envs


if __name__ == '__main__':
    ENVS = read_env_variables()
    print(f'Run with setup: {ENVS}')

    BASE_PATH = Path(ENVS['GRCM_DATA_PATH'])
    VIDEOS_PATH = BASE_PATH / '20bn-jester-v1'
    LABELS_PATH = BASE_PATH / 'annotations'
    TRAIN_LABELS_PATH = BASE_PATH / 'annotations' / 'jester-v1-train.csv'
    VAL_LABELS_PATH = BASE_PATH / 'annotations' / 'jester-v1-validation.csv'

    VIDEOS_LIST = list(os.walk(VIDEOS_PATH))
    MODE = cv2.IMREAD_GRAYSCALE

    print(f'Setup paths: {BASE_PATH} {VIDEOS_PATH} {LABELS_PATH}')
    print(f'Number of videos: {len(VIDEOS_LIST)}')

    labels_train = read_labels(TRAIN_LABELS_PATH)
    labels_val = read_labels(VAL_LABELS_PATH)

    print(f'Labels length: train {len(labels_train)} val {len(labels_val)}')

    print('Loading train dataset...')
    X_train, y_train = create_dataset(VIDEOS_LIST, labels_train, MODE,
                                      limit=ENVS['GRCM_LIMIT_TRAIN'],
                                      class_subset=ENVS['GRCM_CLASS_SUBSET'],
                                      target_shape=ENVS['GRCM_TARGET_SHAPE'],
                                      center_crop=ENVS['GRCM_CENTER_CROP'],
                                      batch_size=ENVS['GRCM_TRAIN_BATCH_SIZE'],
                                      from_preprocessed=ENVS['GRCM_TRAIN_FROM_PREPROCESSED'])
    print('Finished')

    save_to_pkl(X_train, OUTPUT_PATH / 'X_train.pkl')
    save_to_pkl(y_train, OUTPUT_PATH / 'y_train.pkl')

    print('Loading val dataset...')
    X_val, y_val = create_dataset(VIDEOS_LIST, labels_val, MODE,
                                  limit=ENVS['GRCM_LIMIT_VAL'],
                                  class_subset=ENVS['GRCM_CLASS_SUBSET'],
                                  target_shape=ENVS['GRCM_TARGET_SHAPE'],
                                  center_crop=ENVS['GRCM_CENTER_CROP'],
                                  batch_size=None)
    print('Finished')

    save_to_pkl(X_val, OUTPUT_PATH / 'X_val.pkl')
    save_to_pkl(y_val, OUTPUT_PATH / 'y_val.pkl')

    print(f'Shapes:\n'
          f'X_train: {np.shape(X_train)}, y_train: {np.shape(y_train)}\n'
          f'X_val: {np.shape(X_val)}, y_val: {np.shape(y_val)}\n')

    # Classification
    clf_name = ENVS['GRCM_CLF_NAME']
    clf_args = ENVS['GRCM_CLF_ARGS']
    clf = get_classifier(clf_name)(**clf_args)
    clf.fit(X_train, y_train)

    save_to_pkl(clf, OUTPUT_PATH / 'model.pkl')

    y_pred = clf.predict(X_val)

    report = classification_report(y_val, y_pred)
    with open(OUTPUT_PATH / 'results.txt', 'w') as output:
        output.writelines(report)
    np.savetxt(OUTPUT_PATH / 'cf.txt', confusion_matrix(y_val, y_pred), delimiter=',')
    print(report)
