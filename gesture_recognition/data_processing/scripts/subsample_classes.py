import os
import shutil
from typing import List

import click
from tqdm.auto import tqdm

from gesture_recognition.data_processing.io import store_label_names_csv, store_labels_csv_file
from gesture_recognition.data_processing.subsampling import get_dataset_class_subsample
from gesture_recognition.utils.click_utils import LIST_PARAM


@click.command()
@click.option('--train-annotations', type=click.Path(exists=True), required=True,
              help="Training set annotations")
@click.option('--val-annotations', type=click.Path(exists=True), required=True,
              help="Validation set annotations")
@click.option('--videos-path', type=click.Path(exists=True))
@click.option('--new-root', type=click.STRING, required=True)
@click.option('--classes', type=LIST_PARAM, required=True, help="List of classes to be chosen")
@click.option('--force-overwrite', is_flag=True,
              help="Whether to overwrite same name sample when exsists")
def sample_annotations(train_annotations: str, val_annotations: str, videos_path: str,
                       new_root: str, classes: List[str], force_overwrite: bool):
    new_videos_path = os.path.join(new_root, 'videos')
    new_annotations_path = os.path.join(new_root, 'annotations')
    os.makedirs(new_videos_path)
    os.makedirs(new_annotations_path)

    train_subsample = get_dataset_class_subsample(train_annotations, classes)
    store_labels_csv_file(train_subsample,
                          os.path.join(new_annotations_path, os.path.basename(train_annotations)),
                          force_overwrite)

    val_subsample = get_dataset_class_subsample(val_annotations, classes)
    store_labels_csv_file(val_subsample,
                          os.path.join(new_annotations_path, os.path.basename(val_annotations)),
                          force_overwrite)

    store_label_names_csv(classes, os.path.join(new_annotations_path, 'labels.csv'))

    # copy videos to new dir
    all_ids = set(train_subsample['id'].tolist() + val_subsample['id'].tolist())
    for id_ in tqdm(all_ids, desc='Copying videos'):
        shutil.copytree(
            os.path.join(videos_path, str(id_)),
            os.path.join(new_videos_path, str(id_))
        )


if __name__ == '__main__':
    sample_annotations()
