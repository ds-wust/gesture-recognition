import csv
import json
import os
from os import DirEntry
from typing import Any, Dict, Iterator, List, Optional, Tuple, Union

import pandas as pd

from gesture_recognition.settings import TEST_CSV, TRAIN_CSV, TRAIN_SUBSET_NAMES, VAL_CSV


def load_jester_labels(annotation_root_path: str,
                       join_tables: bool = True) -> Union[Tuple[pd.DataFrame, ...], pd.DataFrame]:
    """Provides DataFrame with file ids with corresponding labels from all datasets."""
    train_meta = load_labels_as_dataframe(os.path.join(annotation_root_path, TRAIN_CSV))
    val_meta = load_labels_as_dataframe(os.path.join(annotation_root_path, VAL_CSV))
    test_meta = load_labels_as_dataframe(os.path.join(annotation_root_path, TEST_CSV))

    if join_tables:
        train_meta['dataset'] = 'train'
        val_meta['dataset'] = 'val'
        test_meta['dataset'] = 'test'
        return pd.concat([train_meta, val_meta, test_meta], axis=0)
    else:
        return train_meta, val_meta, test_meta


def load_label_names(csv_path: str) -> List[str]:
    """Loads dataset annotation metadata - list of class names.
    Credit: https://github.com/udacity/CVND---Gesture-Recognition
    """
    classes = []
    with open(csv_path) as csvfile:
        csv_reader = csv.reader(csvfile)
        for row in csv_reader:
            classes.append(row[0])
    return classes


def load_labels_as_dataframe(path: str) -> pd.DataFrame:
    """Loads dataset annotation as pandas.DataFrame."""
    return pd.read_csv(path, sep=';', header=None, names=['id', 'label'])


def store_labels_csv_file(labels: pd.DataFrame, path: str, force: bool = False) -> None:
    """Stores dataset annotation as csv file compliant with original jester format."""
    if os.path.isfile(path) and not force:
        raise ValueError("The given path already exists, use arg 'force' to overwrite!")
    labels.to_csv(path, sep=';', header=False, index=False)


def iter_dataset_by_name_path(videos_root_dir: str,
                              dataset_name: str = None) -> Iterator[Tuple[str, str]]:
    return ((dir_entry.name, dir_entry.path)
            for dir_entry in iter_dataset(videos_root_dir, dataset_name))


def iter_dataset(videos_root_dir: str,
                 dataset_name: str = None) -> Iterator[DirEntry]:
    if dataset_name == 'all':
        return os.scandir(videos_root_dir)
    else:
        dataset_indices = _get_dataset_indices(dataset_name)
        return filter(lambda dir_entry: dir_entry.name in dataset_indices,
                      os.scandir(videos_root_dir))


def get_annotation_files_under_dir(path: str):
    """ Annotation manipulation helper function.
    Checks whether given path contains annotation files having default jester names:
    ['jester-v1-train.csv', 'jester-v1-validation.csv', 'jester-v1-test.csv']
    If the path is file, it is returned as the one-element list.
    """
    if os.path.isdir(path):
        paths = [os.path.join(path, name) for name in TRAIN_SUBSET_NAMES]
        return [pth_ for pth_ in paths if os.path.exists(pth_)]
    elif os.path.isfile(path):
        return [path]
    else:
        raise ValueError(f"Cannot search for annotations under not existing directory: {path}")


def store_label_names_csv(list_: List[Any], path: str):
    with open(path, 'w') as file:
        file.write('\n'.join(list_))


def load_config(path: str) -> Dict:
    """Loads JSON file, primarily purposed to load training configuration"""
    with open(path) as data_file:
        return json.load(data_file)


def _get_dataset_indices(dataset_name: Optional[str]) -> List[str]:
    if dataset_name == 'train':
        return _get_indices(TRAIN_CSV)
    elif dataset_name == 'val':
        return _get_indices(VAL_CSV)
    elif dataset_name == 'test':
        return _get_indices(TEST_CSV)
    else:
        raise ValueError(f"Invalid dataset name: {dataset_name}")


def _get_indices(file_path: str) -> List[str]:
    with open(file_path, 'r') as file:
        return [entry.split(';')[0] for entry in file.read().splitlines()]
