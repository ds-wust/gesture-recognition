from typing import List

import pandas as pd

from gesture_recognition.data_processing.io import load_labels_as_dataframe


def get_dataset_class_subsample(annotations_csv_path: str, selected_classes: List[str]):
    """Loads dataset and provides subset of dataset constrained to given classes"""
    annotations = load_labels_as_dataframe(annotations_csv_path)
    subsample = _filter_annotations_by_class(annotations, selected_classes)
    return subsample


def _filter_annotations_by_class(annotations: pd.DataFrame, selected_classes: List[str]):
    """Filters dataset to obtain subset containing only given classes"""
    return annotations[annotations['label'].isin(selected_classes)]


def _filter_annotations_by_instance_cardinality(annotations: pd.DataFrame, ratio: float,
                                                stratify: bool):
    """Filters dataset to obtain subset with given ratio, optionally stratified"""
    if annotations['label'].isna().sum() == 0 and stratify:
        return annotations.groupby('label').sample(frac=ratio)
    return annotations.sample(frac=ratio)[['id']]
