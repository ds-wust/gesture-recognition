import matplotlib.pyplot as plt


def get_simple_axis(width: int = 14, height: int = 6):
    _, ax = plt.subplots(figsize=(width, height))
    return ax


def rotate_xticklabels(axes, angle=45):
    axes = make_iter(axes)
    for ax in axes:
        for label in ax.get_xticklabels():
            label.set_ha("right")
            label.set_rotation(angle)


def make_iter(obj):
    try:
        iter(obj)
    except TypeError:
        obj = [obj]
    return obj
