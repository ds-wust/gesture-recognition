"""
Main script running the training process of gesture recognition model on Jester dataset
"""

import click
from pytorch_lightning import Trainer
from pytorch_lightning import loggers as pl_loggers
from pytorch_lightning.callbacks import EarlyStopping, LearningRateMonitor, ModelCheckpoint

from gesture_recognition.data_processing.io import load_config, load_label_names
from gesture_recognition.datasets.jester_datamodule import JesterDataModule
from gesture_recognition.deep_model.architectures import get_model
from gesture_recognition.settings import W_AND_B_PROJECT_NAME
from gesture_recognition.utils.logging import get_logger


@click.command()
@click.option('--experiment-name', '-n', type=click.STRING, default='test_experiment')
@click.option('--config-path', '-c', type=click.Path(exists=True), required=True,
              help='json config file path')
@click.option('--log-dir', '-l', type=click.Path(), required=True,
              help="Path to store tensorboard logs")
@click.option('--tpu-cores', type=click.INT, default=None)
@click.option('--gpus', type=click.INT, default=None)
@click.option('--checkpoint-path', '-c', type=click.STRING, default=None)
@click.option('--use-tensorboard', is_flag=True)
@click.option('--use-wandb', is_flag=True)
def train(experiment_name: str, config_path: str, log_dir: str, tpu_cores: int, gpus: int,
          checkpoint_path: str, use_tensorboard: bool, use_wandb: bool):
    logger.info(locals())
    config = load_config(config_path)
    logger.info(config)
    jester_ds = JesterDataModule(config)

    training_config = config['training']

    checkpoint_callback = ModelCheckpoint(
        monitor='val_f1',
        filename=experiment_name + '-{epoch:02d}-{val_f1:.2f}',
        save_top_k=training_config['k_checkpoints'],
        mode='max'
    )

    early_stopping_callback = EarlyStopping(
        monitor='val_f1',
        min_delta=training_config['es_min_delta_f1'],
        patience=training_config['es_patience'],
        mode='max'
    )

    lr_logger_callback = LearningRateMonitor(logging_interval='step')

    trainer = Trainer(
        default_root_dir=log_dir,
        logger=_get_loggers(log_dir, experiment_name, use_tensorboard, use_wandb),
        max_epochs=training_config['max_epochs'],
        tpu_cores=tpu_cores,
        resume_from_checkpoint=checkpoint_path,
        gpus=gpus,
        callbacks=[checkpoint_callback, early_stopping_callback, lr_logger_callback],
        num_sanity_val_steps=0,
    )

    class_names_path = config['paths']['labels_csv']
    class_names = load_label_names(class_names_path)
    model = get_model(config['model'], config=config['hyperparameters'],
                      class_names=class_names)
    trainer.fit(model, datamodule=jester_ds)


def _get_loggers(log_dir: str, experiment_name: str, use_tensorboard: bool, use_wandb: bool):
    loggers = []

    if use_tensorboard:
        loggers.append(pl_loggers.TensorBoardLogger(save_dir=log_dir, name=experiment_name))

    if use_wandb:
        loggers.append(pl_loggers.WandbLogger(save_dir=log_dir, name=experiment_name,
                                              project=W_AND_B_PROJECT_NAME, log_model=True))

    # CSV logger interferes with other loggers and make logs dir structure ambiguous
    if not loggers:
        loggers.append(pl_loggers.CSVLogger(save_dir=log_dir, name=experiment_name))

    return loggers


if __name__ == '__main__':
    logger = get_logger(__name__)
    train()
