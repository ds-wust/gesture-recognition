"""
Gesture recognition model training as LightningModule capable of optimize arbitrary torch.nn.Module,
which is compliant with provided data from Jester dataset
"""
from abc import ABC
from typing import Dict, List, Tuple

import pytorch_lightning as pl
import torch.nn.functional as func_nn
from pytorch_lightning.metrics.classification import F1, Accuracy, ConfusionMatrix
from torch import nn
from torch.optim import Adam
from torch.optim.lr_scheduler import LambdaLR, MultiStepLR

from gesture_recognition.utils.logging import get_logger
from gesture_recognition.utils.metrics import get_confusion_matrix


class GestureClassifier(pl.LightningModule, ABC):
    """Training procedure for gesture recognition models on Jester dataset."""

    def __init__(self, config: Dict, class_names: List[str], *args, **kwargs):
        super().__init__(*args, **kwargs)
        config['class_names'] = class_names
        self.save_hyperparameters(config)
        self.class_names = class_names

        self.config = config
        self.loss = nn.CrossEntropyLoss()

        self.train_acc = Accuracy()
        self.val_acc = Accuracy()
        self.train_f1 = F1(self.config['num_classes'], average='macro')
        self.val_f1 = F1(self.config['num_classes'], average='macro')
        self.confusion_matrix = ConfusionMatrix(num_classes=self.config['num_classes'],
                                                normalize='true')

        self._logger = get_logger('GestureRecognition')

    @property
    def num_classes(self):
        return len(self.class_names)

    def shared_step(self, batch):
        x, y = batch
        logits = self.forward(x)
        loss = func_nn.cross_entropy(logits, y)
        return logits, loss

    def training_step(self, train_batch, batch_idx):
        x_train, y_train = train_batch
        logits, loss = self.shared_step(train_batch)

        self.train_acc(logits, y_train)
        self.train_f1(logits, y_train)
        self.log('train_loss', loss, on_step=False, on_epoch=True, prog_bar=True)
        self.log('train_accuracy', self.train_acc, on_step=False, on_epoch=True)
        self.log('train_f1', self.train_f1, on_step=False, on_epoch=True)
        return loss

    def validation_step(self, val_batch, batch_idx):
        x, y = val_batch
        logits, loss = self.shared_step(val_batch)

        self.val_acc(logits, y)
        self.val_f1(logits, y)
        self.log('val_loss', loss, on_step=False, on_epoch=True)
        self.log('val_accuracy', self.val_acc, on_step=False, on_epoch=True, prog_bar=True)
        self.log('val_f1', self.val_f1, on_step=False, on_epoch=True, prog_bar=True)

    def on_train_end(self) -> None:
        cm = self.confusion_matrix.compute()
        try:
            self.logger.experiment[0].add_figure('val_confusion_matrix',
                                                 get_confusion_matrix(cm, self.class_names),
                                                 global_step=self.global_step)
        except Exception as err:
            self._logger.warning("Unexpected error has occurred "
                                 f"while trying to log confusion matrix: {err} - omitting")

    def configure_optimizers(self):
        """Prepares optimizers for training network.

        The chosen optimizer is Adam. The method allows for using dynamic learning rate:
        - 'warmup': uses given epochs of warmup steps before starting to use normal LR
        - 'multi_step': decreases LR exactly at specified epochs

        Credits for gradual warmup:
            https://github.com/PyTorchLightning/pytorch-lightning/issues/328#issuecomment-644120903
        """
        optimizer = Adam(self.parameters(),
                         lr=self.hparams.learning_rate,
                         weight_decay=self.hparams.weight_decay)

        if self.hparams.lr_scheduler == 'warmup':
            scheduler = LambdaLR(optimizer, lr_lambda=self.gradual_lr_schedule)
        elif self.hparams.lr_scheduler == 'multi_step':
            scheduler = MultiStepLR(optimizer, milestones=self.hparams.multi_milestones,
                                    gamma=self.hparams.multi_gamma)
        else:
            return optimizer

        return [optimizer], [scheduler]

    def gradual_lr_schedule(self, epoch):
        if epoch < self.hparams.warmup_steps:
            lr_scale = 0.1 ** (self.hparams.warmup_steps - epoch)
        else:
            lr_scale = 0.95 ** epoch

        return lr_scale

    def get_conv_layer(self,
                       in_c: int,
                       out_c: int,
                       pool_size: Tuple[int, ...],
                       pool_stride: Tuple[int, ...],
                       kernel_size=3):
        """Provides 3D-CNN block with batch normalization, activation and max pooling"""
        return nn.Sequential(
            nn.Conv3d(in_c, out_c, kernel_size=kernel_size, stride=1, padding=1),
            nn.BatchNorm3d(out_c),
            self.get_activation(),
            nn.MaxPool3d(pool_size, stride=pool_stride, padding=0)
        )

    def get_activation(self):
        activation_name = self.hparams.get('activation', 'elu').strip().lower()
        if activation_name == 'elu':
            return nn.ELU()
        elif activation_name == 'relu':
            return nn.ReLU()
        elif activation_name == 'leaky_relu':
            return nn.LeakyReLU()
