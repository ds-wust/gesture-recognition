"""
Deep neural network architectures for video classification on Jester dataset
"""
from typing import Dict, List

import torch.nn as nn

from gesture_recognition.deep_model.base import GestureClassifier


class ConvColumn(GestureClassifier):
    """CNN with 3D-convolutions modelling spatio-temporal relations
    Credit: https://github.com/udacity/CVND---Gesture-Recognition

    Default linear layer size refers to images cropped to 84x84
    """

    def __init__(self, config: Dict, class_names: List[str], *args, **kwargs):
        super().__init__(config, class_names, *args, **kwargs)

        self.conv_layers = nn.Sequential(
            self.get_conv_layer(3, 64, (1, 2, 2), (1, 2, 2)),
            self.get_conv_layer(64, 128, (2, 2, 2), (2, 2, 2)),
            self.get_conv_layer(128, 256, (2, 2, 2), (2, 2, 2)),
            self.get_conv_layer(256, 256, (2, 2, 2), (2, 2, 2)),
        )

        self.fc_1 = nn.Linear(12800, 512)
        self.fc_1_act = self.get_activation()
        self.fc_out = nn.Linear(512, self.num_classes)

    def forward(self, x):
        x = self.conv_layers(x)

        x = x.view(x.size(0), -1)

        x = self.fc_1(x)
        x = self.fc_1_act(x)

        x = self.fc_out(x)
        return x


class ConvColumnLargeFilter(GestureClassifier):
    """ConvColumn architecture with higher and changing kernel size."""

    def __init__(self, config: Dict, class_names: List[str], *args, **kwargs):
        super().__init__(config, class_names, *args, **kwargs)

        self.conv_layers = nn.Sequential(
            self.get_conv_layer(3, 64, (1, 2, 2), (1, 2, 2), 3),
            self.get_conv_layer(64, 128, (2, 2, 2), (2, 2, 2), 5),
            self.get_conv_layer(128, 256, (2, 2, 2), (2, 2, 2), 3),
            self.get_conv_layer(256, 256, (2, 2, 2), (2, 2, 2), 5),
        )

        self.fc_1 = nn.Linear(4096, 512)
        self.fc_1_act = self.get_activation()
        self.fc_out = nn.Linear(512, self.num_classes)

    def forward(self, x):
        x = self.conv_layers(x)

        x = x.view(x.size(0), -1)

        x = self.fc_1(x)
        x = self.fc_1_act(x)

        x = self.fc_out(x)
        return x


class ConvColumnSmall(GestureClassifier):
    """Smaller version of ColumnConv network"""

    def __init__(self, config: Dict, class_names: List[str], *args, **kwargs):
        super().__init__(config, class_names, *args, **kwargs)

        self.conv_layers = nn.Sequential(
            self.get_conv_layer(3, 32, (1, 2, 2), (1, 2, 2)),
            self.get_conv_layer(32, 64, (2, 2, 2), (2, 2, 2)),
            self.get_conv_layer(64, 128, (2, 2, 2), (2, 2, 2)),
            self.get_conv_layer(128, 128, (2, 2, 2), (2, 2, 2)),
        )
        self.fc_1 = nn.Sequential(
            nn.Linear(6400, 2048),
            nn.ELU(),
            nn.Dropout(0.5),
        )

        self.fc_2 = nn.Sequential(
            nn.Linear(2048, 512),
            nn.ELU(),
            nn.Dropout(0.5)
        )

        self.fc_out = nn.Linear(512, self.num_classes)

    def forward(self, x):
        x = self.conv_layers(x)

        x = x.view(x.size(0), -1)

        x = self.fc_1(x)
        x = self.fc_2(x)
        x = self.fc_out(x)
        return x


def get_model(name: str, *args, **kwargs):
    if name == 'conv_column':
        return ConvColumn(*args, **kwargs)
    elif name == 'conv_column_kernel':
        return ConvColumnLargeFilter(*args, **kwargs)
    elif name == 'conv_column_small':
        return ConvColumnSmall(*args, **kwargs)
    elif name == 'conv_lstm':
        raise NotImplementedError
    else:
        raise ValueError(f"Invalid network architecture name: {name}")
