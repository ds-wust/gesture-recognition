"""
PyTorch datasets for Jester - provides online loading prevents memory overloads.
Credit: https://github.com/udacity/CVND---Gesture-Recognition
"""

import csv
import glob
import itertools
import os
import random
from dataclasses import dataclass
from math import nan
from typing import Any, Callable, Dict, List, Union

import numpy as np
import torch
import torch.utils.data
from PIL import Image

from gesture_recognition.data_processing.io import load_label_names
from gesture_recognition.datasets.torch_transforms import ImgaugVideoAugmentationTransform


@dataclass
class ListDataJpeg:
    id: str
    label: str
    path: str


class VideoDataset(torch.utils.data.Dataset):
    """Class represents single video as a folder of JPEG frames

    The class provides interface to videos from Jester dataset, loaded when requested
    """

    IMG_EXTENSIONS = ['.jpg', '.JPG', '.jpeg', '.JPEG']

    def __init__(self,
                 root: str,
                 csv_file_input: str,
                 csv_file_labels: str,
                 clip_size: int,
                 n_clips: int,
                 step_size: int,
                 is_val: bool,
                 transform: Any = None,
                 padding_type: str = 'last',
                 augmentation_proba: float = False,
                 ):
        """
        :param root: Path to folder containing videos (stored in original Jester format)
        :param csv_file_input: Path to CSV specifying subset of videos from 'root' path
        :param csv_file_labels: Path to CSV specifying labels for videos
        :param clip_size: Number of frames that represents a single unit of video
        :param n_clips: Number of clips, which are taken from videos
        :param step_size: Step size at which frames are taken
        :param is_val: Validation set indicator (setting to False prevents temporal augmentation)
        :param transform: List of torchvision transforms applied to data
        :param padding_type: Type of the padding used, one of ['last', 'recursive_repeat']
        """
        self.dataset_object = JpegDataset(csv_file_input, csv_file_labels, root)

        self.csv_data = self.dataset_object.csv_data
        self.classes = self.dataset_object.classes
        self.classes_dict = self.dataset_object.classes_dict
        self.root = root
        self.transform = transform

        self.clip_size = clip_size
        self.nclips = n_clips
        self.step_size = step_size
        self.is_val = is_val
        self.padding_type = padding_type

        self.augmentation_proba = augmentation_proba
        if augmentation_proba:
            self.aug_transforms = ImgaugVideoAugmentationTransform()

    def __getitem__(self, index):
        item = self.csv_data[index]
        img_paths = self._get_frame_names(item.path)

        images = []
        for img_path in img_paths:
            img = Image.open(img_path).convert('RGB')
            images.append(np.array(img))

        if self.augmentation_proba and random.uniform(0, 1) <= self.augmentation_proba:
            images = self.aug_transforms(images)

        images = [torch.unsqueeze(self.transform(img), 0) for img in images]

        target_idx = self.classes_dict.get(item.label, nan)

        # format data to torch
        data = torch.cat(images)
        data = data.permute(1, 0, 2, 3)
        return data, target_idx

    def __len__(self):
        return len(self.csv_data)

    @property
    def labels(self):
        """Provides labels of entire dataset (in order) as tensor of longs."""
        labels = [self.classes_dict[item.label] for item in self.csv_data]
        return torch.as_tensor(labels, dtype=torch.long)

    def get_indices_of_labels(self, query_class_names: List[str]):
        """Provides indices at which given labels occurs"""
        query_labels = [self.classes_dict[item] for item in query_class_names]
        return [idx for idx, label in enumerate(self.labels) if label in query_labels]

    def get_path(self, index: int):
        return self.csv_data[index].path

    def _get_frame_names(self, path: str):
        """"Provides paths to frames from video at the given path chosen to be used in the dataset.

        The frames are sampled according to specified parameters, number of frames is determined
        with the following equation:
            num_frames_necessary = self.clip_size * self.nclips * self.step_size

        If there is no sufficient frames in the folder, the padding of choice is applied:
            - 'last': last frame is repeated at the end of the original video
            - 'recursive_repeat': each frame is repeated, as in
                https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=8803152

        If the number of frames exceeds number of necessary frames:
            - training set: random temporal chunk is taken (as a kind of data augmentation)
            - validation set: first num_frames_necessary are taken
        """
        frame_names = []
        for ext in self.IMG_EXTENSIONS:
            frame_names.extend(glob.glob(os.path.join(path, "*" + ext)))
        frame_names = list(sorted(frame_names))
        num_frames = len(frame_names)

        # set number of necessary frames
        if self.nclips > -1:
            num_frames_necessary = self.clip_size * self.nclips * self.step_size
        else:
            num_frames_necessary = num_frames

        # pick frames
        offset = 0
        if num_frames_necessary > num_frames:
            padding_func = self._get_padding_func()
            frame_names = padding_func(frame_names, num_frames_necessary, num_frames)
            # pad last frame if video is shorter than necessary
            frame_names += [frame_names[-1]] * (num_frames_necessary - num_frames)

        if num_frames_necessary < num_frames:
            # If there are more frames, then sample starting offset
            diff = (num_frames - num_frames_necessary)
            # Temporal augmentation
            if not self.is_val:
                offset = np.random.randint(0, diff)

        frame_names = frame_names[offset:num_frames_necessary + offset:self.step_size]
        return frame_names

    def _get_padding_func(self) -> Callable[[List[str], int, int], List[str]]:
        if self.padding_type == 'last':
            return self._pad_last_frame
        elif self.padding_type == 'recursive_repeat':
            return self._pad_recursive_repeat
        else:
            raise ValueError(f"Invalid type for video frame padding: {self.padding_type}")

    @staticmethod
    def _pad_last_frame(frame_names: List[str], num_frames_necessary: int,
                        num_frames: int) -> List[str]:
        """Performs padding when padding_type = 'last'"""
        frame_names += [frame_names[-1]] * (num_frames_necessary - num_frames)
        return frame_names

    @staticmethod
    def _pad_recursive_repeat(frame_names: List[str], num_frames_necessary: int,
                              *args, **kwarg) -> List[str]:
        """Performs padding when padding_type = 'recursive_repeat'

        Source: https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=8803152
        """
        n_repeats = num_frames_necessary // len(frame_names)
        return list(
            itertools.chain.from_iterable(
                itertools.repeat(frame, n_repeats) for frame in frame_names
            )
        )


class JpegDataset:
    """Metadata of concrete Jester dataset instance"""

    def __init__(self, csv_path_input: str, csv_path_labels: str, data_root: str):
        self.classes = load_label_names(csv_path_labels)
        self.classes_dict = self.get_two_way_dict(self.classes)
        self.csv_data = self.read_csv_input(csv_path_input, data_root)

    def read_csv_input(self, csv_path: str, data_root: str) -> List[ListDataJpeg]:
        """Reads CSV file, which contains metadata for the Jester dataset."""
        csv_data = []
        with open(csv_path) as csvfile:
            csv_reader = csv.reader(csvfile, delimiter=';')
            for row in csv_reader:
                try:
                    video_id, label = row
                except ValueError:
                    video_id, *_ = row
                    label = None

                item = ListDataJpeg(video_id,
                                    label,
                                    os.path.join(data_root, video_id)
                                    )
                if label is None or label in self.classes:
                    csv_data.append(item)
        return csv_data

    @staticmethod
    def get_two_way_dict(classes: List[str]) -> Dict[Union[int, str], Union[int, str]]:
        """Provides two-way mapping, suitable for mapping label names into indices."""
        classes_dict = {}
        for i, item in enumerate(classes):
            classes_dict[item] = i
            classes_dict[i] = item
        return classes_dict
