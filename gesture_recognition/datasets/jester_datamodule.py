"""
Pytorch Lightning DataModules for Jester dataset
"""

from typing import Any, Dict, List, Optional, Union

import pytorch_lightning as pl
import torch.utils.data
from torch.utils.data import BatchSampler, DataLoader, RandomSampler, WeightedRandomSampler

from gesture_recognition.datasets.torch_dataset import VideoDataset
from gesture_recognition.datasets.torch_sampler import CurriculumBatchSampler
from gesture_recognition.datasets.torch_transforms import get_transform


class JesterDataModule(pl.LightningDataModule):
    """Base for Jester dataset. Provides dataset without any preprocessing."""

    def __init__(self, config: Dict, transform: Optional[Any] = None):
        super().__init__()
        self.hp_config = config['hyperparameters']
        self.path_config = config['paths']

        if transform is None:
            self.transform = get_transform(self.hp_config['preprocessing'],
                                           self.hp_config['resize_method'],
                                           self.hp_config['image_size'])
        else:
            self.transform = transform

        self.train_data = None
        self.val_data = None

    def setup(self, stage: Optional[str] = None):
        self.train_data = VideoDataset(root=self.path_config['train_data_folder'],
                                       csv_file_input=self.path_config['train_data_csv'],
                                       csv_file_labels=self.path_config['labels_csv'],
                                       clip_size=self.hp_config['clip_size'],
                                       n_clips=self.hp_config['nclips'],
                                       step_size=self.hp_config['step_size'],
                                       is_val=False,
                                       transform=self.transform,
                                       padding_type=self.hp_config['padding_type'],
                                       augmentation_proba=self.hp_config['augmentation_proba'],
                                       )
        self.val_data = VideoDataset(root=self.path_config['val_data_folder'],
                                     csv_file_input=self.path_config['val_data_csv'],
                                     csv_file_labels=self.path_config['labels_csv'],
                                     clip_size=self.hp_config['clip_size'],
                                     n_clips=self.hp_config['nclips'],
                                     step_size=self.hp_config['step_size'],
                                     is_val=True,
                                     transform=self.transform,
                                     padding_type=self.hp_config['padding_type'],
                                     augmentation_proba=0,
                                     )

    def train_dataloader(self) -> DataLoader:
        if self.hp_config.get('balance_weights', False):
            labels = self.train_data.labels
            label_weights = 1. / torch.bincount(labels)
            sampler_weights = label_weights[labels]

            sampler = WeightedRandomSampler(sampler_weights,
                                            num_samples=len(sampler_weights),
                                            replacement=False)
        else:
            sampler = RandomSampler(self.train_data)

        if self.hp_config.get('curriculum'):
            batch_sampler = CurriculumBatchSampler(
                sampler,
                batch_size=self.hp_config['batch_size'],
                drop_last=False,
                curriculum_indices=self.train_data.get_indices_of_labels(
                    self.hp_config['curriculum_labels']),
                curriculum_epochs=self.hp_config['curriculum_epochs']
            )
        else:
            batch_sampler = BatchSampler(
                sampler,
                batch_size=self.hp_config['batch_size'],
                drop_last=False,
            )

        return DataLoader(
            self.train_data,
            batch_sampler=batch_sampler,
            num_workers=self.hp_config['num_workers'],
            pin_memory=True,
        )

    def val_dataloader(self) -> Union[DataLoader, List[DataLoader]]:
        return DataLoader(self.val_data,
                          batch_size=self.hp_config['batch_size'],
                          shuffle=False,
                          num_workers=self.hp_config['num_workers'],
                          pin_memory=True,
                          drop_last=False)
