import torch
from torch.utils.data import BatchSampler, Sampler


class CurriculumBatchSampler(BatchSampler):
    """Custom sampler to allow for easy curriculum learning."""

    def __init__(self, sampler: Sampler[int], batch_size: int, drop_last: bool,
                 curriculum_indices: torch.Tensor, curriculum_epochs: int):
        super().__init__(sampler, batch_size, drop_last)
        self.curriculum_indices = curriculum_indices
        self.curriculum_epochs = curriculum_epochs
        self.finished_epochs = 0

    def __iter__(self):
        """Reimplements original __iter__ to filter out examples outside the curriculum_indices.
        Note that this will result in varying number of batches in epoch
        """
        batch = []
        for idx in self.sampler:
            if (self.finished_epochs < self.curriculum_epochs
                    and idx not in self.curriculum_indices):
                continue
            batch.append(idx)
            if len(batch) == self.batch_size:
                yield batch
                batch = []
        if len(batch) > 0 and not self.drop_last:
            yield batch

        self.finished_epochs += 1
