from typing import List, Optional

import imgaug.augmenters as iaa
import numpy as np
from torchvision.transforms import CenterCrop, Compose, Normalize, Resize, ToTensor

from gesture_recognition.datasets.parameters import MEAN_IMAGE, STD_IMAGE


def get_transform(preprocessing: Optional[str],
                  resize_method: str,
                  image_size: int) -> Compose:
    """Provides transform preparing image to deep model."""
    if preprocessing is None or preprocessing == 'default':
        processing_steps = [
            ToTensor(),
            get_image_resizer(resize_method, image_size)
        ]

    elif preprocessing == 'crop_normalize':
        processing_steps = [
            ToTensor(),
            get_image_resizer(resize_method, image_size),
            Normalize(mean=MEAN_IMAGE, std=STD_IMAGE)
        ]
    else:
        raise ValueError(f"Invalid transformation type: {preprocessing}")

    return Compose(processing_steps)


def get_image_resizer(resize_method: str, target_size: int):
    """Provides method for image resizing"""
    if resize_method == 'crop':
        return CenterCrop(target_size)
    elif resize_method == 'scale':
        return Resize(target_size)
    else:
        raise ValueError(f"Invalid resize method name: {resize_method}")


class ImgaugVideoAugmentationTransform:
    """Video augmentation with the usage of imgaug library."""

    def __init__(self):
        noise_augmenters = iaa.OneOf(
            children=[
                iaa.ImpulseNoise(0.1),
                iaa.SaltAndPepper(0.1, per_channel=True),
                iaa.AdditiveGaussianNoise(scale=(0, 0.2 * 255)),
                iaa.GaussianBlur(sigma=(0.0, 3.0)),
            ],
        )

        color_augmenters = iaa.OneOf(
            children=[
                iaa.AddToBrightness((-30, 30)),
                iaa.UniformColorQuantization(),
                iaa.Grayscale(alpha=(0.0, 1.0)),
            ]
        )

        deformation_augmenters = iaa.SomeOf(
            n=(1, 3),
            children=[
                iaa.Affine(rotate=(-20, 20)),
                iaa.Affine(scale=(0.5, 1.5)),
                iaa.Affine(shear=(-16, 16)),
            ],
        )

        self.augmenters = iaa.SomeOf(
            n=(1, 3),
            children=[
                noise_augmenters,
                color_augmenters,
                deformation_augmenters,
            ]
        )

    def __call__(self, video: List[np.ndarray]) -> List[np.ndarray]:
        aug_det = self.augmenters.to_deterministic()
        return [aug_det.augment_image(image) for image in video]
