import os

from definitions import PROJECT_PATH

DATA_PATH = os.path.join(PROJECT_PATH, 'data')
DEEP_MODELS_PATH = os.path.join(DATA_PATH, 'deep_models')
JESTER_PATH = os.path.join(DATA_PATH, 'jester')

CONFIG_PATH = os.path.join(PROJECT_PATH, 'config')
QUICK_TEST_CONFIG = os.path.join(CONFIG_PATH, 'quick_testing_config.json')
TRAIN_CONFIG = os.path.join(CONFIG_PATH, 'train_config.json')

SUBSET_NAMES = ['jester-v1-train.csv', 'jester-v1-validation.csv', 'jester-v1-test.csv']
TRAIN_SUBSET_NAMES = ['jester-v1-train.csv', 'jester-v1-validation.csv']
TRAIN_CSV = 'jester-v1-train.csv'
VAL_CSV = 'jester-v1-validation.csv'
TEST_CSV = 'jester-v1-test.csv'
LABELS_CSV = 'jester-v1-labels.csv'

W_AND_B_PROJECT_NAME = 'gesture_recognition'

SELECTED_CLASSES = [
    'Stop Sign',
    'Swiping Left',
    'No gesture',
    'Thumb Up',
    'Thumb Down',
    'Swiping Right',
    'Shaking Hand',
]
