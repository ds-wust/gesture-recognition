from abc import ABC, abstractmethod
from typing import List

from gesture_recognition.utils.logging import get_logger


class PredictorBase(ABC):
    def __init__(self):
        self._logger = get_logger(type(self).__name__)

    @abstractmethod
    def predict(self, *args, **kwargs) -> List[int]:
        """Prediction logic returning class indices."""

    @property
    @abstractmethod
    def class_names(self) -> List[str]:
        """List of class names."""

    @property
    @abstractmethod
    def num_frames(self) -> int:
        """Number of frames required by the model."""
