import os

from gesture_recognition.predictors.base_predictor import PredictorBase
from gesture_recognition.predictors.deep_predictor import ConvColumnPredictor


def get_predictor(model_name: str, checkpoint_path: str) -> PredictorBase:
    if model_name == 'ConvColumn':
        return ConvColumnPredictor(checkpoint_path)
    else:
        raise ValueError(f"Cannot find model type which satisfies name: {model_name}")


def try_parse_camera_index(camera_index: str):
    try:
        return int(camera_index)
    except ValueError:
        if os.path.isfile(camera_index):
            return camera_index
        else:
            raise
