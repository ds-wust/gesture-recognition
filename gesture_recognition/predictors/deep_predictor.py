from abc import ABC
from math import ceil
from typing import List, Optional, Tuple, Union

import numpy as np
import torch
from torch.nn.functional import softmax
from torch.utils.data.dataloader import DataLoader
from tqdm.auto import tqdm

from gesture_recognition.datasets.torch_dataset import VideoDataset
from gesture_recognition.datasets.torch_transforms import get_transform
from gesture_recognition.deep_model.architectures import ConvColumn
from gesture_recognition.predictors.base_predictor import PredictorBase
from gesture_recognition.predictors.definitions import CLASS_NAMES

T_videos = List[List[np.ndarray]]
T_tuple_list_int_float = Tuple[List[int], List[float]]


class DLPredictor(PredictorBase, ABC):
    """Predictor based on deep-learning model"""

    def __init__(self, model_checkpoint_path: str, device: Optional[str] = None):
        super().__init__()
        self.checkpoint_path = model_checkpoint_path

        self.device = device or ('cuda' if torch.cuda.is_available() else 'cpu')
        self.model = None

    @property
    def hyper_params(self):
        """Provides access to hyperparameters used to train the model"""
        return self.model.hparams

    @property
    def class_names(self):
        return self.model.class_names


class ConvColumnPredictor(DLPredictor):
    def __init__(self, model_checkpoint_path: str, device: Optional[str] = None):
        super().__init__(model_checkpoint_path, device)
        self.model = ConvColumn.load_from_checkpoint(self.checkpoint_path,
                                                     class_names=CLASS_NAMES).eval()
        self.model.freeze()
        self.model.to(self.device)

        try:
            self.preprocessor = get_transform(self.hyper_params.preprocessing,
                                              self.hyper_params.resize_method,
                                              self.hyper_params.image_size)
        except AttributeError as err:
            raise AttributeError("Not enough data contained in checkpoint") from err

    @property
    def num_frames(self) -> int:
        return self.hyper_params.nclips * self.hyper_params.clip_size

    def predict(self, data: Union[T_videos, VideoDataset],
                skip_processing: bool = False,
                batch_size: Optional[int] = None,
                verbose: bool = False,
                ret_confidence: bool = False,
                *args,
                **kwargs) -> Union[List[int], T_tuple_list_int_float]:
        """Predicts label for sequence of images.

        Prediction might be done in the following ways:
            - images packed into 4D matrix (matrix of videos)
            - in an automated batched manner, using VideoFolder dataset
        """

        if batch_size is not None and skip_processing:
            raise ValueError("Cannot use preprocessing when using batched prediction")

        if batch_size is not None:
            label_ids, labels_confidence = self._predict_from_dataset(data, batch_size, verbose)

        else:
            if not skip_processing:
                data = self._prepare(data)

            label_ids, labels_confidence = self._predict_from_batch(data)

        if ret_confidence:
            return label_ids, labels_confidence
        else:
            return label_ids

    def _predict_from_dataset(self, data: VideoDataset,
                              batch_size: int,
                              verbose: bool) -> T_tuple_list_int_float:
        """Performs prediction from static dataset holding own preprocessing logic."""

        if verbose:
            self._logger.info(f"Predicting dataset with #examples: {len(data)}")

        loader = DataLoader(data, batch_size=batch_size, shuffle=False)

        label_ids = []
        labels_confidence = []

        iter_over_batches = tqdm(loader, total=(ceil(len(data) / batch_size)),
                                 disable=not verbose)
        for videos, *_ in iter_over_batches:
            batch_label_ids, batch_labels_confidence = self._predict_from_batch(videos)
            label_ids.extend(batch_label_ids)
            labels_confidence.extend(batch_labels_confidence)

        return label_ids, labels_confidence

    def _prepare(self, data: T_videos) -> torch.Tensor:
        """Prepares raw videos to be compatible with the network input."""
        processed_videos = []
        for video in data:
            processed_images = torch.stack([self.preprocessor(image) for image in video])
            processed_videos.append(processed_images.permute(1, 0, 2, 3))

        processed_videos = torch.stack(processed_videos)
        return processed_videos

    def _predict_from_batch(self, data: torch.Tensor) -> T_tuple_list_int_float:
        """Performs network forward pass to obtain prediction for single batch of videos."""
        data = data.to(self.device)
        output = softmax(self.model(data), dim=1)
        output = output.cpu()

        label_ids = output.argmax(dim=1).tolist()
        labels_confidence = output[range(len(label_ids)), label_ids].tolist()
        return label_ids, labels_confidence
