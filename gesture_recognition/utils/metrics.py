from typing import List, Tuple

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import torch

TupleOfInt = Tuple[int, ...]


def get_confusion_matrix(conf_mat: [torch.Tensor, np.ndarray],
                         class_names: List[str],
                         fig_size: Tuple[int, int] = (9, 6)) -> plt.Figure:
    """Credit: https://onestopdataanalysis.com/confusion-matrix-python/"""
    if isinstance(conf_mat, torch.Tensor):
        conf_mat = conf_mat.numpy().round(2)
    else:
        conf_mat = conf_mat.round(2)
    sns.set(color_codes=True)
    fig = plt.figure(1, figsize=fig_size)

    sns.set(font_scale=0.5)
    ax = sns.heatmap(conf_mat, annot=True, cmap="YlGnBu")

    ax.set_xticklabels(class_names)
    ax.set_yticklabels(class_names)
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right", rotation_mode="anchor")
    plt.setp(ax.get_yticklabels(), rotation=-60, ha="left", rotation_mode="anchor")

    ax.set(ylabel="True Label", xlabel="Predicted Label")
    return fig


def compute_conv_layer_output_shape(input_dim: TupleOfInt, padding: TupleOfInt,
                                    dilation: TupleOfInt, stride: TupleOfInt,
                                    kernel_size: TupleOfInt) -> TupleOfInt:
    """Based on: https://pytorch.org/docs/master/generated/torch.nn.Conv3d.html#conv3d"""
    return tuple(
        1 + (dim + 2 * pad - dil * (kern - 1) - 1) // s
        for dim, pad, dil, kern, s in zip(input_dim, padding, dilation, kernel_size, stride)
    )
