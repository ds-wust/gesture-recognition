import ast

import click


class ListParam(click.ParamType):
    name = 'list_param'

    def convert(self, value, param, ctx):
        try:
            if isinstance(value, list):
                return value
            return list(ast.literal_eval(value))
        except Exception as err:
            self.fail(f"{value!r} is not a valid list: {err}", param, ctx)


LIST_PARAM = ListParam()
