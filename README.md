# Gesture Recognition

Gesture recognition based on [Jester Dataset](https://20bn.com/datasets/jester).

Authors:

- Jakub Binkowski
- Mateusz Wójcik

[[_TOC_]]

## Demo

The demo application is available provided that you have trained models. The easiest way to run demo, is to use publicly
available docker image:

```bash
docker run --rm \
--device=/dev/video0 \
-e DISPLAY=$DISPLAY \
--volume /tmp/.X11-unix/:/tmp/.X11-unix  \
binkjakub/gesturedemo:latest \
--camera-index 0
```

Note that this will run only with Linux. The script above mounts a video device available at index 0. Also, there is
volume for displaying output video with classification. You can also utilize GPU with `--gpus` arg passed to docker run.

If you want to run demo locally with own custom model , just use the script as shown below:

```bash
python -m demo.scripts.run_demo --camera-index <index_or_file_name> --model-name ConvColum --checkpoint-path <path_to_trained_model
```

## Installation

Use the package manager and install all pre-defined requirements.

```bash
$ pip install -r requirements.txt
```

For development purposes use development requirements as well. Then, run sanity check with `pytest`:

```bash
$ pip install -r requirements.txt -r requirements-dev.txt
$ pytest
```

## Data

The dataset used in the training was [Jester Dataset](https://20bn.com/datasets/jester) containing videos classified to
27 gestures. The dataset is provided to everyone on academic license for free. Also, this repository has `dvc`
repository attached, and hence having access to the remote storage will allow to download a zipped dataset after
executing `dvc pull` in terminal.

Additionally, `dvc` repository contains several checkpoints with trained weights of the model, which are essential to
run demo application.

**ATTENTION** Extracting dataset in the scope of the project might be inefficient and lead to IDE (e.g. PyCharm) crash;
Dataset contain large number of files and should be extracted outside this project.

## Usage - deep learning model training

Deep model is based on neural network, which is trained based on provided configuration. The configuration is loaded
from the file specified as a command arg (see command description below). There is a default config provided with this
repo at
[train_config.json](config/train_config.json). Also, for quick testing purposes you can utilize
the [quick_testing_config.json](config/quick_testing_config.json) file.

### Parameters

- `paths`
    - `train_data_folder` - path to folder with the videos
    - `val_data_folder` - path to folder with the videos (in Jester dataset, exactly the same as above)
    - `train_data_csv` - path to file specifying train set items (`jester-v1-train.csv`)
    - `val_data_csv` - path to file specifying validation set items (`jester-v1-validation.csv`)
    - `labels_csv` - path to file specifying list of class names (`jester-v1-labels.csv`)
- `model` - type of model's architecture; one of [`conv_column`, `conv_column_kernel`, `conv_column_small`]
- `hyperparameters`
    - `activation` - activation function name; one of [`relu`, `elu`, `leaky_relu`]
    - `balance_weights` - whether to use sampler providing equal class distribution across each batch
    - `preprocessing` - type of preprocessing; one of [`crop_normalize`, `default`]
    - `resize_method` - type of resieze method; one of [`crop`, `rescale`]
    - `image_size` - target image size used in `resize_method`; recommended is `84`
    - `augmentation_proba` - probability of artificially augmenting each video, 0 indicates no augmentation
    - `num_workers` - number of workers used in data loader
    - `num_classes` - number of classes (`27` when using full dataset)
    - `batch_size` - number of videos in single batch
    - `padding_type` - type of method for filling missing frames in videos; one of [`last`, `recursive_repeat`]
    - `clip_size` - number of frames that represents a single unit of video
    - `nclips` - number of clips, which are taken from videos
    - `step_size` - step size at which frames are taken
    - `learning_rate` - learning rate passed to Adam optimzier
    - `weight_decay` - weight decay regularization of Adam optimizer
    - `lr_scheduler` - type of learning rate scheduler, `false` determines no scheduling; one
      of [ `false`, `warmup`, `multi_step`]
    - `warmup_steps` - number of warmup steps when using `lr_scheduler=warmup`
    - `multi_milestones` - epochs at which learning rate is rescaled when using `lr_scheduler=multi_step`
    - `multi_gamma` - rate of learning rate scaling at each milestone when using `lr_scheduler=multi_step`
    - `curriculum` - whether to use curriculum learning (start with training on labels specified below through some
      number epochs)
    - `curriculum_labels` - list of labels used in curriculum learning, must match those defined in `labels_csv`
    - `curriculum_epochs` - number of epochs, in which only data labeled by `curriculum_labels` is used
- `training`
    - `max_epochs` - maximal number of epochs;
    - `es_min_delta_f1` - minimum increase in F1 to continue learning
    - `es_patience` - allowed number of epochs without F1 increase
    - `k_checkpoints` - number of best checkpoints to store

--- 
### Training
#### Training on Google Colab

You can utilize provided notebooks on Google Colab runtimes to access free GPU training - follow the steps described in
the [train_model.ipynb notebook](colab/train_model.ipynb). Note that this might be problematic due to Colab constraints.

#### Training natively on own machine

For running the training procedure locally, provided that you have configuration filled at `<config_path>`:

```bash
python -m gesture_recognition.deep_model.scripts.train \
    --experiment-name test_experiment \
    --config-path <config_path> \
    --log-dir <logs_dir> \
    --gpus 1 \
    --use-tensorboard \
    --use-wandb
```

**ATTENTION 1** Training is written with dynamic data loader and should not cause memory error. However, it is strongly
recommended running it on GPU, e.g. on Tesla K80 it takes about a day to finish single experiment.

**ATTENTION 2** When using Weight&Biases logger, which pushes logs to remote server, you have to log in to your
account (`wandb login`) before starting experiment.

**ATTENTION 3** You can get the help from training script by typing
`python -m gesture_recognition.deep_model.scripts.train --help`

#### Training with Docker

You can utilize Docker to train the network. You have to only provide two or three volumes:

- `local_jester_dataset_path`: volume with downloaded dataset (should be the root of dataset with `annotations`
  and `videos` folder inside)
- `logs_placeholder`: volume for experiment logs
- `config`: you mount volume with configuration to use custom parameters

You can run training with publicly available docker image, almost just like on a local machine:

```bash
 docker run --rm \
 --volume <local_jester_dataset_path>:/data/jester \
 --volume <logs_placeholder>:/data/jester_logs \
 binkjakub/gesture_deep:latest \
--experiment-name <your_experiment_name> \
--config-path ./config/quick_testing_config.json \
--log-dir /data/jester_logs \
--use-tensorboard
```
You can also utilize GPU with `--gpus` arg passed to docker run.