FROM python:3.8

ADD .. / gesture-recognition/
WORKDIR gesture-recognition

RUN apt-get update && apt-get install ffmpeg libsm6 libxext6  --yes
RUN pip install --no-cache-dir -r requirements.txt

RUN mkdir --parents /data/jester
RUN mkdir --parents /data/jester_logs

ENTRYPOINT ["python", "-m", "gesture_recognition.deep_model.scripts.train"]
