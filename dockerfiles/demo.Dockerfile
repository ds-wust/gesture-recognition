FROM python:3.8

ADD .. / gesture-recognition/
COPY ../data/deep_models/3d_full_jester_crop_normalize_scheduled_multistep_lr_recursive_padding_augmentation-epoch=05-val_f1=0.78.ckpt ./data/models/
WORKDIR gesture-recognition

RUN apt-get update && apt-get install ffmpeg libsm6 libxext6  --yes
RUN pip install --no-cache-dir -r requirements.txt

ENTRYPOINT ["python", "-m", "demo.scripts.run_demo.py", "--model-name", "ConvColumn", "--checkpoint-path", "/data/models/3d_full_jester_crop_normalize_scheduled_multistep_lr_recursive_padding_augmentation-epoch=05-val_f1=0.78.ckpt"]