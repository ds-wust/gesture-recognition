import ast
import os

from pathlib import Path

PROJECT_PATH = Path(__file__).resolve().parent
DATA_PATH = PROJECT_PATH / 'data'
EXAMPLE_DATA_PATH = DATA_PATH / 'examples' / 'videos'

if ast.literal_eval(os.getenv('GRCM_IS_INSIDE_DOCKER', default="False")):
    OUTPUT_PATH = Path('/output')
    TRAIN_PKL_PATH = Path('/storage')
else:
    OUTPUT_PATH = PROJECT_PATH / 'output'
    TRAIN_PKL_PATH = DATA_PATH / 'train'
