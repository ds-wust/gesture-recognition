from typing import Optional

import click

from demo.camera import RealTimeCamera
from gesture_recognition.predictors.factory import get_predictor, try_parse_camera_index


@click.command()
@click.option('--camera-index', type=click.STRING, default=0,
              help="Index of camera device or video file path")
@click.option('--model-name', type=click.Choice(['ConvColumn'], case_sensitive=False),
              default='ConvColumn',
              help="Name of the model, which weights are loaded from 'checkpoint path'")
@click.option('--checkpoint-path', type=click.Path(exists=True, dir_okay=False), required=True,
              help="Path to the checkpoint containing trained weights for the 'model'")
@click.option('--fps', type=click.INT, default=10,
              help="Frequency at which prediction is made")
@click.option('--prediction-frequency', type=click.INT, default=1,
              help="Frequency at which prediction is made")
@click.option('--output-path', type=click.Path(dir_okay=False), default=None,
              help="Optional path ath which detection will be dumped")
def run_demo(camera_index: str, model_name: str, checkpoint_path: str, fps: 10,
             prediction_frequency: int, output_path: Optional[str]):
    camera_index = try_parse_camera_index(camera_index)
    predictor = get_predictor(model_name, checkpoint_path)
    camera = RealTimeCamera(predictor, camera_index, fps, prediction_frequency,
                            video_path=output_path)
    camera.run()


run_demo()
