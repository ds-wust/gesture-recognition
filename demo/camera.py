import time
from collections import deque
from time import sleep
from typing import Optional, Union

import cv2
from PIL import Image

from gesture_recognition.predictors.base_predictor import PredictorBase
from gesture_recognition.utils.logging import get_logger


class RealTimeCamera:
    """Real time video capture with classification."""
    READ_TIMEOUT = 0.1
    WRITE_TIMEOUT = 0.1
    FONT = cv2.FONT_HERSHEY_SIMPLEX

    POST_PRED_SLEEP_SECONDS = 4

    def __init__(self, predictor: PredictorBase, camera_index: Union[int, str], fps: int,
                 step: int = 2, prediction_frequency: int = 1, video_path: Optional[str] = None):
        self.fps = fps
        self.step = step
        self.camera_index = camera_index
        self.video_path = video_path

        self.buffer_len = predictor.num_frames
        self.max_pred_duration = 1 / prediction_frequency

        self.predictor = predictor

        self._logger = get_logger(__name__)

    def run(self) -> None:
        """Starts reading consecutive frames from camera and sending to prediction."""

        label_name = None
        confidence = None

        last_frame_time = time.time()
        last_pred_time = time.time()

        frame_buffer = deque(maxlen=self.buffer_len)

        video_capture = cv2.VideoCapture(self.camera_index)

        if self.video_path:
            fourcc = cv2.VideoWriter_fourcc(*'MP4V')
            video_out = cv2.VideoWriter(self.video_path, fourcc, self.fps, (640, 480))

        current_step = 0
        global_step = 0

        while True:
            last_frame_duration = time.time() - last_frame_time
            last_frame_time = time.time()

            current_fps = int(1 / last_frame_duration)
            if current_fps > self.fps + 1:
                sleep(1 / self.fps)
                continue

            self._logger.debug("Current fps: %d", current_fps)

            ret, orig_frame = video_capture.read()

            if ret is False:
                print("Camera disconnected or not recognized")
                break

            self._logger.debug("Frame size: %s", str(orig_frame.shape))

            frame = cv2.cvtColor(orig_frame, cv2.COLOR_BGR2RGB)
            frame = Image.fromarray(frame)

            if current_step % self.step == 0:
                frame = frame.resize((128, 128))
                frame_buffer.append(frame)
                current_step = 1
            else:
                current_step += 1

            last_pred_duration = time.time() - last_pred_time
            if (len(frame_buffer) >= self.buffer_len
                    and last_pred_duration >= self.max_pred_duration):
                last_pred_time = time.time()

                (label, *_), (confidence, *_) = self.predictor.predict([frame_buffer],
                                                                       ret_confidence=True)
                label_name = self.predictor.class_names[label]
                confidence = confidence

            if label_name:
                self._logger.info("[LABEL_NAME: %s, CONFIDENCE: %f]", label_name, confidence)

                orig = (0, 20)
                cv2.putText(orig_frame, f"{label_name}:{confidence: 0.2f}", orig,
                            self.FONT, 0.5,
                            (0, 0, 255), 2, cv2.LINE_AA)

            cv2.imshow("Gesture recognition demo", orig_frame)

            if global_step == 2:
                sleep(2)
            global_step += 1

            if self.video_path:
                video_out.write(orig_frame)

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

        video_capture.release()
        if self.video_path:
            video_out.release()

        sleep(self.POST_PRED_SLEEP_SECONDS)
        cv2.destroyAllWindows()
